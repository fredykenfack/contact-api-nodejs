const { contact } = require('../db/sequelize')

module.exports = (app) => {
    app.get('/api/contacts', (req, res) => {
        contact.findAll()
            .then(contacts => {
                const message = 'La liste des contacts a bien été récupérée.'
                res.json({ message, data: contacts })
            })
    })
}