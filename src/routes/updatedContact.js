const { contact } = require('../db/sequelize')

module.exports = (app) => {
    app.put('/api/contacts/update/:id', (req, res) => {
        const id = req.params.id
        contact.update(req.body, {
            where: { id: id }
        })
            .then(_ => {
                contact.findByPk(id).then(c => {
                    const message = `Le contact ${c.name} a bien été modifié.`
                    res.json({message, data: c })
                })
            })
    })
}