const { contact } = require('../db/sequelize')

module.exports = (app) => {
    app.delete('/api/contacts/delete/:id', (req, res) => {
        contact.findByPk(req.params.id).then(c => {
            const contactDeleted = c;
            c.destroy({
                where: { id: c.id }
            })
                .then(_ => {
                    const message = `Le contact avec l'identifiant n°${contactDeleted.id} a bien été supprimé.`
                    res.json({message, data: contactDeleted })
                })
        })
    })
}