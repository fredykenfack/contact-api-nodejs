const { contact } = require('../db/sequelize')

module.exports = (app) => {
    app.post('/api/contacts/create', (req, res) => {
        contact.create(req.body)
            .then(c => {
                const message = `Le contact ${req.body.name} a bien été crée.`
                res.json({ message, data: c })
            })
    })
}