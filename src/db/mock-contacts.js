const contacts = [
    {
        id: 1,
        name: "Gilgamesh kenfack",
        pays: "cameroun",
        ville: "Douala",
        email: "fk@gmail.com",
        loisirs: ["sport", "lecture"],
        telephone: "697663515"
    },
    {
        id: 2,
        name: "Touo tsabze Laure",
        pays: "cameroun",
        ville: "Yaounde",
        email: "tl@gmail.com",
        loisirs: ["veille technologique"],
        telephone: "690518000"
    },
    {
        id: 3,
        name: "nnah nnang marion",
        pays: "cameroun",
        ville: "Yaounde",
        email: "ar@gmail.com",
        loisirs: ["lecture"],
        telephone: "693087271"
    },
    {
        id: 4,
        name: "Tonleu Justine",
        pays: "cameroun",
        ville: "Douala",
        email: "tj@gmail.com",
        loisirs: ["sport", "lecture", "natation"],
        telephone: "699233320"
    },
    {
        id: 5,
        name: "fofou tryphene sahyd",
        pays: "cameroun",
        ville: "Douala",
        email: "fts@gmail.com",
        loisirs: [],
        telephone: "690802070"
    }
];
module.exports = contacts