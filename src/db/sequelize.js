const { Sequelize, DataTypes } = require('sequelize')
const contactModel = require('../models/contact')
const contacts = require('./mock-contacts')

const sequelize = new Sequelize('contacts', 'root', 'root', {
    host: 'localhost',
    //port: 8889,
    dialect: 'mysql',
    dialectOptions: {
        timezone: 'UTC',
    },
    logging: false
})

const contact = contactModel(sequelize, DataTypes)

const initDb = () => {
    return sequelize.sync({force: true}).then(_ => {
        contacts.map(c => {
            contact.create({
                name: c.name,
                pays: c.pays,
                ville: c.ville,
                email: c.email,
                loisirs: c.loisirs.toString(),
                telephone: c.telephone
            }).then(fred => console.log(fred.toJSON()))
        })
        console.log('La base de donnée a bien été initialisée !')
    })
}

module.exports = {
    initDb, contact
}