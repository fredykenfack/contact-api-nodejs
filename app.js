const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const cors = require('cors')
const sequelize = require('./src/db/sequelize')

const app = express()
const port = 3000;

app
    .use(morgan('dev'))
    .use(bodyParser.json())
    .use(cors({origin: "*"}))

sequelize.initDb()

//Ici serons placer nos futur point de terminaison

require('./src/routes/findAllContacts')(app)

require('./src/routes/findContactById')(app)

require('./src/routes/createContact')(app)

require('./src/routes/updatedContact')(app)

require('./src/routes/deleteContact')(app)

app.listen(port, () => { console.log(`Notre application tourne sur: http://localhost:${port}`) })