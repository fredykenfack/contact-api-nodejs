exports.success = (message, data) => {
    return {message, data}
}

exports.getUniqueId = (contacts) => {
    const contactsIds = contacts.map(contact => contact.id)
    const maxId = contactsIds.reduce((a,b) => Math.max(a, b))
    return (maxId+1)
}